package com.estech.gatosmvvm.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.estech.gatosmvvm.data.modelos.enviarvoto.SendVote
import com.estech.gatosmvvm.data.modelos.listagatos.Breed
import com.estech.gatosmvvm.data.Repositorio
import com.estech.gatosmvvm.data.modelos.enviarvoto.ResponseVote
import com.estech.gatosmvvm.data.network.ApiError
import com.estech.gatosmvvm.data.network.ApiLoading
import com.estech.gatosmvvm.data.network.ApiResult
import com.estech.gatosmvvm.data.network.ApiSuccess
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/**
 * Created by sergi on 11/05/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class GatosViewModel : ViewModel() {

    private val repositorio = Repositorio()
    val selectedCat = MutableLiveData<Breed>()

    fun getListaGatos(): MutableLiveData<ApiResult<ArrayList<Breed>>> {
        val liveData = MutableLiveData<ApiResult<ArrayList<Breed>>>()
        liveData.postValue(ApiLoading())
        CoroutineScope(Dispatchers.IO).launch {

            val response = repositorio.getRazas()

            if (response.isSuccessful) {
                val listaRazas = response.body()
                listaRazas?.let {
                    liveData.postValue(ApiSuccess(it))
                }
            } else {
                liveData.postValue(ApiError(response.code(), response.message()))
            }
        }
        return liveData
    }

    fun votaGato(sendVote: SendVote): MutableLiveData<ApiResult<ResponseVote>> {
        val exitoVoto = MutableLiveData<ApiResult<ResponseVote>>()
        exitoVoto.postValue(ApiLoading())
        CoroutineScope(Dispatchers.IO).launch {
            val response = repositorio.sendVote(sendVote)

            if (response.isSuccessful) {
                val respuesta = response.body()
                respuesta?.let {
                    val estado = ApiSuccess(response.body()!!)
                    exitoVoto.postValue(estado)

                }
            } else {
                exitoVoto.postValue(ApiError(response.code(), response.message()))
            }
        }
        return exitoVoto
    }

    fun selectCat(breed: Breed) {
        selectedCat.value = breed
    }
}