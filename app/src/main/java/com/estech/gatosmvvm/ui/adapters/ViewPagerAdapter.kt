package com.estech.gatosmvvm.ui.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.estech.gatosmvvm.ui.fragments.InfoFragment
import com.estech.gatosmvvm.ui.fragments.StatsFragment
import com.estech.gatosmvvm.data.modelos.listagatos.Breed


/**
 * Created by sergi on 11/05/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class ViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return if (position == 0)
            InfoFragment()
        else
            StatsFragment()
    }
}