package com.estech.gatosmvvm.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.estech.gatosmvvm.ui.MainActivity
import com.estech.gatosmvvm.ui.adapters.ViewPagerAdapter
import com.estech.gatosmvvm.databinding.FragmentDetalleGatoBinding
import com.estech.gatosmvvm.data.modelos.enviarvoto.SendVote
import com.estech.gatosmvvm.data.network.ApiError
import com.estech.gatosmvvm.data.network.ApiLoading
import com.estech.gatosmvvm.data.network.ApiSuccess
import com.estech.gatosmvvm.ui.viewmodels.GatosViewModel
import com.google.android.material.tabs.TabLayoutMediator

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class GatoDetalleFragment : Fragment() {

    private lateinit var binding: FragmentDetalleGatoBinding
    private val vm: GatosViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentDetalleGatoBinding.inflate(inflater, container, false)
        (requireActivity() as MainActivity).changeToolbar(binding.toolbar, true)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm.selectedCat.observe(viewLifecycleOwner) { raza ->
            (requireActivity() as MainActivity).changeToolbarTitle(raza!!.name!!)

            if (raza.image != null && raza.image.url != null) {
                Glide.with(this).load(raza.image.url).into(binding.catImg)
            }

            binding.megusta.setOnClickListener { votaGato(1, raza.image?.id!!) }

            binding.nomegusta.setOnClickListener { votaGato(0, raza.image?.id!!) }

//            vm.exitoVoto.observe(viewLifecycleOwner) {
//
//            }
//
//            vm.errorVoto.observe(viewLifecycleOwner) {
//                Toast.makeText(requireContext(), "Error: $it", Toast.LENGTH_SHORT).show()
//            }

            binding.fab.setOnClickListener {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(raza.wikipediaUrl)))
            }

            setupViewPager()
        }
    }

    private fun setupViewPager() {
        val adapter = ViewPagerAdapter(this)
        binding.materialupViewpager.adapter = adapter
        val mediator = TabLayoutMediator(
            binding.tabs, binding.materialupViewpager
        ) { tab, position ->
            if (position == 0) {
                tab.text = "Information"
            } else {
                tab.text = "Stats"
            }
        }
        mediator.attach()
    }

    private fun votaGato(voto: Int, imageId: String) {
        val sendVote = SendVote(imageId, voto, "Sergio")

        vm.votaGato(sendVote).observe(viewLifecycleOwner){
            binding.progressBar2.isVisible = false
            when(it) {
                is ApiError -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
                is ApiLoading -> {
                    binding.progressBar2.isVisible = true
                }
                is ApiSuccess -> {
                    Toast.makeText(requireContext(), it.data.message, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

}