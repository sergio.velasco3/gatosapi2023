package com.estech.gatosmvvm.data.network

sealed interface ApiResult<T : Any>

class ApiSuccess<T : Any>(var data: T) : ApiResult<T>
class ApiError<T : Any>(var code: Int, var message: String) : ApiResult<T>
class ApiLoading<T : Any>() : ApiResult<T>

