package com.estech.gatosmvvm

import android.app.Application
import com.estech.gatosmvvm.data.Repositorio

class MyApp: Application() {

    private val repositorio by lazy { Repositorio()}
}